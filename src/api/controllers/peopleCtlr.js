const fetchAll = require('../../utils/fetchAll');
const features = require('../../utils/apiFeatures');

const getAllPeople = async (req, res) => {
  await fetchAll('/people').then((data) => {
    features.sortList(data, req.query.sortBy);
    res.status(200).json({
      status: 'Success',
      total: data.length,
      people: data
    });
  });
};

module.exports = { getAllPeople };
