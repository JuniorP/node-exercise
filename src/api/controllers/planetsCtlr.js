const axios = require('axios');
const fetchAll = require('../../utils/fetchAll');
const features = require('../../utils/apiFeatures');

const getAllPlanets = async (req, res) => {
  const planets = await fetchAll('/planets');

  await Promise.all(
    planets.map(async (planet) => {
      const resident = await Promise.all(
        planet.residents.map(async (i) => axios.get(i))
      );
      const residents = resident.reduce(
        (acc, cur) => [...acc, cur.data.name],
        []
      );
      return { ...planet, residents };
    })
  ).then((data) => {
    features.sortList(data);
    res.status(200).json({
      status: 'success',
      total: data.length,
      planets: data
    });
  });
};

module.exports = { getAllPlanets };
