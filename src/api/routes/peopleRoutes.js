const express = require('express');
const peopleCtlr = require('../controllers/peopleCtlr');

const router = express.Router();

router.route('/').get(peopleCtlr.getAllPeople);

module.exports = router;
