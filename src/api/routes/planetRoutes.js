const express = require('express');
const planetsCtlr = require('../controllers/planetsCtlr');

const router = express.Router();

router.route('/').get(planetsCtlr.getAllPlanets);

module.exports = router;
