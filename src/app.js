const express = require('express');
const peopleRouter = require('./api/routes/peopleRoutes');
const planetRouter = require('./api/routes/planetRoutes');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/people', peopleRouter);
app.use('/planets', planetRouter);

app.all('*', (req, res, next) => {
  const error = new Error('Not Found');
  error.status = 404;
  next(error);
});

// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    status: error.status,
    message: error.message || 'Internal Server Error'
  });
});

module.exports = app;
