const axios = require('axios');

const getPagesList = (totalCount, pageCount) => {
  const availablePages = [];
  let count = totalCount - pageCount;
  let page = 2;
  while (count > 0) {
    availablePages.push(page);
    page += 1;
    count -= pageCount;
  }
  return availablePages;
};

const fetchAll = async (path) => {
  const baseURL = 'https://swapi.dev/api';
  let starwarsPeople = [];

  // first page
  return (
    axios
      .get(`${baseURL}${path}`)
      .then((response) => {
        starwarsPeople = response.data.results;
        return getPagesList(response.data.count, starwarsPeople.length);
      })
      // remaining pages
      .then((availablePages) =>
        Promise.all(
          availablePages.map(async (i) =>
            axios.get(`${baseURL}${path}/?page=${i}`)
          )
        )
      )
      .then((response) =>
        response.reduce(
          (acc, cur) => [...acc, ...cur.data.results],
          [...starwarsPeople]
        )
      )
  );
};

module.exports = fetchAll;
