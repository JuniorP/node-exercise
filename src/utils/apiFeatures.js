// convert property val & handle 'unknown' by returning as 0
const propValToInt = (string) => parseInt(string, 10) || 0;

const compareSort = (resource, query) =>
  resource.sort((a, b) => {
    const h1 = propValToInt(a[query]);
    const h2 = propValToInt(b[query]);
    return h1 - h2;
  });

const compareNames = (a, b) => {
  if (a.name < b.name) {
    return -1;
  }
  if (a.name > b.name) {
    return 1;
  }
  return 0;
};

const sortList = (resource, query) => {
  switch (query) {
    case 'height':
      compareSort(resource, query);
      break;
    case 'mass':
      compareSort(resource, query);
      break;
    default:
      resource.sort(compareNames);
  }
};

module.exports = { sortList };
